package jaspret.addvalsolutions.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import jaspret.addvalsolutions.R;
import jaspret.addvalsolutions.http.AsyncTaskListener;
import jaspret.addvalsolutions.http.PostCall;
import jaspret.addvalsolutions.http.Urls;
import jaspret.addvalsolutions.util.Decription;
import jaspret.addvalsolutions.util.ViewUtil;

/**
 * Created by jaspret on 01/05/16.
 */
public class VerifyEmailFragment extends Fragment implements AsyncTaskListener {

    private static final int REGISTER_EMAIL=1;
    private static final int VERIFY_EMAIL=2;

    private EditText        mEmail;
    private Button          btSubmit;

    private String TAG=this.getClass().getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_email, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEmail=(EditText)view.findViewById(R.id.email);
        btSubmit=(Button)view.findViewById(R.id.btSubmit);
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitClick(v);
            }
        });
    }

    protected boolean checkAllFields() {
        if (mEmail.getText().toString().isEmpty()) {
            mEmail.setFocusable(true);
            mEmail.setError(getString(R.string.email_empty));
            return false;
        } else if (!isEmailValid(mEmail.getText())) {
            mEmail.setFocusable(true);
            mEmail.setError(getString(R.string.not_valid));
            return false;
        }

        return true;
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }

    @Override
    public void onHttpResponse(String callResponse, int pageId) {
        Log.i(TAG,callResponse);

//        {"success":true,"message":"Thank you for signing up","encrypted_message":"laieufbtlesiuif"}
        if(pageId==REGISTER_EMAIL) {
            try {
                JSONObject jsonObject = new JSONObject(callResponse);
                if(jsonObject.getBoolean("success")) {
                    String enc = jsonObject.getString("encrypted_message");
                    String dec = Decription.getDecPassword(enc);
                    JSONObject test = new JSONObject();
                    JSONObject data = new JSONObject();
                    data.put("email", mEmail.getText().toString());
                    data.put("decrypted_message", dec);
                    data.put("encrypted_message", enc);
                    test.put("test", data);
                    PostCall postCall = new PostCall(Urls.VERIFY_EMAIL, test, null, this, VERIFY_EMAIL);
                    postCall.execute();
                }
                ViewUtil.showDialog(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(VERIFY_EMAIL==pageId){
            try {
                JSONObject jsonObject = new JSONObject(callResponse);
                if (jsonObject.getBoolean("success")) {
                    openRegistration();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        ViewUtil.closeDialog();
    }


    private void openRegistration(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.abc_fade_out,R.anim.abc_fade_out);
        Fragment registerFragment = new RegisterFragment();
        Bundle bundle=new Bundle();
        bundle.putString("email",mEmail.getText().toString());
        registerFragment.setArguments(bundle);
        ft.replace(R.id.container, registerFragment, "registerFragment").addToBackStack("registerFragment");
        ft.commit();
    }
    @Override
    public void onError(String error, int pageId) {
        ViewUtil.closeDialog();
    }


    public void onSubmitClick(View v){

        if(!checkAllFields())
            return;
        try {
            JSONObject email = new JSONObject();
            email.put("email", mEmail.getText().toString());
            JSONObject request=new JSONObject();
            request.put("user",email);
            PostCall postCall = new PostCall(Urls.REGISTER_USER,request,null,this,REGISTER_EMAIL );
            postCall.execute();
            ViewUtil.showDialog(getContext());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
