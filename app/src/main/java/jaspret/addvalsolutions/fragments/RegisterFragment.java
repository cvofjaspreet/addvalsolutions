package jaspret.addvalsolutions.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import org.json.JSONObject;

import jaspret.addvalsolutions.R;
import jaspret.addvalsolutions.http.AsyncTaskListener;
import jaspret.addvalsolutions.http.PostCall;
import jaspret.addvalsolutions.http.PutCall;
import jaspret.addvalsolutions.http.Urls;
import jaspret.addvalsolutions.util.ViewUtil;

/**
 * Created by jaspret on 01/05/16.
 */
public class RegisterFragment extends Fragment implements AsyncTaskListener{

    private static final int REGISTER=1;

    private boolean         android=true;

    private EditText        etName,etOrganisation,etTotalExp;

    private RadioButton     rdAndroid;

    private Button          btSubmit;

    private String          TAG=this.getClass().getName();

    public boolean isAndroid() {
        return android;
    }

    public void setAndroid(boolean android) {
        this.android = android;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etName=(EditText) view.findViewById(R.id.etName);
        etOrganisation=(EditText) view.findViewById(R.id.etOrganisation);
        etTotalExp=(EditText) view.findViewById(R.id.etTotalExp);
        rdAndroid=(RadioButton) view.findViewById(R.id.rdAndroid);
        btSubmit=(Button) view.findViewById(R.id.btSubmit);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitClick(v);
            }
        });

        rdAndroid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setAndroid(isChecked);
            }
        });
    }

    @Override
    public void onHttpResponse(String callResponse, int pageId) {

        Log.v(TAG,callResponse);
        try{
        JSONObject jsonObject=new JSONObject(callResponse);
        if(jsonObject.getBoolean("success")){
            openThanks();
        }
        }catch (Exception e){
            e.printStackTrace();
        }
        ViewUtil.closeDialog();

    }

    @Override
    public void onError(String error, int pageId) {
        ViewUtil.closeDialog();
    }


    public void onSubmitClick(View v){

        if(!checkAllFields())
            return;
        try {
            JSONObject user = new JSONObject();
            user.put("email", getArguments().getString("email"));
            user.put("name",etName.getText().toString());
            user.put("gender","male");
            user.put("phone_number","");
            user.put("current_company",etOrganisation.getText().toString());
            user.put("experience",etTotalExp.getText().toString());
            user.put("area_of_expertise",(isAndroid())?"java":"iOS");
            user.put("comments","");

            JSONObject request=new JSONObject();
            request.put("user",user);
            PutCall postCall = new PutCall(Urls.REGISTER,request,null,this,REGISTER );
            postCall.execute();
            ViewUtil.showDialog(getContext());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void openThanks(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.abc_fade_out,R.anim.abc_fade_out);
        Fragment thanksFragment = new ThanksFragment();
        ft.replace(R.id.container, thanksFragment, "thanksFragment").addToBackStack("thanksFragment");
        ft.commit();
    }
    protected boolean checkAllFields() {
        if (etName.getText().toString().isEmpty()) {
            etName.setFocusable(true);
            etName.setError(getString(R.string.name_empty));
            return false;
        } else  if (etOrganisation.getText().toString().isEmpty()) {
            etOrganisation.setFocusable(true);
            etOrganisation.setError(getString(R.string.organisation_empty));
            return false;
        } else  if (etTotalExp.getText().toString().isEmpty()) {
            etTotalExp.setFocusable(true);
            etTotalExp.setError(getString(R.string.ex_empty));
            return false;
        }

        return true;
    }
}
