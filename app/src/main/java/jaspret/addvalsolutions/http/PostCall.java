package jaspret.addvalsolutions.http;
/**
 @auther Jaspreet Singh Chhabra

 Senior software Developer

 Wegile, Mohali Branch
 **/

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class PostCall extends AsyncTask<Void, Void, JSONObject> {
	private String url;
	private JSONObject request;
	private AsyncTaskListener callListener;
	private int pageId;
	private ProgressDialog dialog;
	private int timeOut=10*1000;
	public PostCall(String url, JSONObject request, ProgressDialog dialog, AsyncTaskListener callListener, int pageId) {
		this.url=url;
		this.request=request;
		this.callListener=callListener;
		this.pageId = pageId;
		this.dialog=dialog;
		}
	public PostCall(String url, JSONObject request, ProgressDialog dialog, AsyncTaskListener callListener, int pageId,int timeOut) {
		this.url=url;
		this.request=request;
		this.callListener=callListener;
		this.pageId = pageId;
		this.dialog=dialog;
		this.timeOut=timeOut;
	}
	@Override
	protected JSONObject doInBackground(Void... params) {

		try {
				HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
				con.setRequestMethod("POST");
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestProperty("Content-Type", "application/json");
				con.setConnectTimeout(timeOut);
				con.setReadTimeout(timeOut);
				con.getOutputStream().write(request.toString().getBytes(StandardCharsets.UTF_8));
				int status = con.getResponseCode();


				if (status == HttpURLConnection.HTTP_OK)
			    {
			         String response_string=convertToString(con.getInputStream());
					 JSONObject jsonObject=new JSONObject() ;
					 jsonObject.put("response_string",response_string);
					 jsonObject.put("error",false);
					return jsonObject;
			    }
			    else
				{
					String response_string=convertToString(con.getErrorStream());
					JSONObject jsonObject=new JSONObject() ;
					jsonObject.put("response_string",response_string);
					jsonObject.put("error",true);
					return jsonObject;

			    }
				} catch (Exception e) {

					JSONObject jsonObject = new JSONObject();
					JSONObject error=new JSONObject();
					try {
						error.put("errorMsg", e.getMessage());
						jsonObject.put("response_string",error);
						jsonObject.put("error", true);
					}catch (Exception e1){
						e1.printStackTrace();
					}
					return jsonObject;
				}
	}
	private String convertToString(InputStream content) throws Exception {
		
			InputStreamReader inputStreamReader=new InputStreamReader(content);
			BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
			String s="";
			StringBuffer buffer=new StringBuffer();
			while ((s=bufferedReader.readLine())!=null) {
				buffer.append(s);
			}
				
		return buffer.toString();
	}
	@Override
	protected void onPostExecute(JSONObject result) {
if( callListener!= null){
	super.onPostExecute(result);
	try {
			if(!result.getBoolean("error")){

					callListener.onHttpResponse(result.getString("response_string"), pageId);
			}else{
				callListener.onError(result.getString("response_string"), pageId);
			}
			if(this.dialog!=null){
			dialog.dismiss();
			}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
		cancel(true);
		
		
	
	}	

}
