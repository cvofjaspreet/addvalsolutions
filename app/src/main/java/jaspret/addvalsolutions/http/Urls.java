package jaspret.addvalsolutions.http;


/**
 * Created by jaspret on 08/04/16.
 */
public interface Urls {

    String REGISTER_USER ="https://infinite-spire-5111.herokuapp.com/api/v1/sign_up";
    String VERIFY_EMAIL="https://infinite-spire-5111.herokuapp.com/api/v1/submit";
    String REGISTER="https://infinite-spire-5111.herokuapp.com/api/v1/register";

}
