/**
@auther Jaspreet Singh Chhabra

Senior software Developer 

Wegile, Mohali Branch
**/
package jaspret.addvalsolutions.http;


public interface AsyncTaskListener {
	public void onHttpResponse(String callResponse, int pageId);
	public void onError(String error, int pageId);
}
