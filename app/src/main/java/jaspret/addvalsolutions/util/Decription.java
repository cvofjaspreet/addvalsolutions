package jaspret.addvalsolutions.util;

/**
 * Created by jaspret on 30/04/16.
 */
public class Decription {


    private static int ROW;
    private static int COL;
    public static String getDecPassword(String enc){
        char[] encPassword=enc.toCharArray();
        char[] decPassword=new char[encPassword.length];
        int incLength=encPassword.length;
        COL=incLength+1;
        ROW=5;
        int z=0;
        char ar[][]=new char[ROW][COL];

        for(int i=0;i<ROW;i++){
            for(int j=0,k=0,l=0;j<COL;j++,k++){
                if(j%8==0){
                    k=0;
                    l++;
                }

                ar[i][j]=(k==i) || (j==l*8-i)?(z<incLength)?(j==(COL-1)&& z>=incLength-getRemainingItems(i, j))?'*':encPassword[z++]:'*':'*';
//                System.out.print(ar[i][j]);
            }
//            System.out.print("\n");

        }

        z=0;

        for(int i=0;i<COL;i++){
            for(int j=0;j<ROW;j++){
                if(ar[j][i]!='*')
                    decPassword[z++]=ar[j][i];
            }
        }


        return (new String(decPassword));

    }
    private static int getRemainingItems(int i,int j ){
        int m=j/9;
        return (7-i*2)*(m+1);
    }
}
