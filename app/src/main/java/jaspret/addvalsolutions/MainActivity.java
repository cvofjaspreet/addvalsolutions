package jaspret.addvalsolutions;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import jaspret.addvalsolutions.fragments.VerifyEmailFragment;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openVeriFyEmail();
    }


   private void openVeriFyEmail(){
       FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
       ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.abc_fade_out,R.anim.abc_fade_out);
       Fragment verifyEmailFragment = new VerifyEmailFragment();
       ft.add(R.id.container, verifyEmailFragment, "verifyEmailFragment");
       ft.commit();
    }






}
